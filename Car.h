#import <Foundation/Foundation.h>
@class Engine;
@class Tire;
@interface Car : NSObject <NSCopying>
{
    NSMutableArray * tires;
    Engine * engine;
}
@property (retain) Engine * engine;

-(void)setTire:(Tire *) tire atIndex:(int)index;
-(Tire *)tireAtIndex:(int)index;

-(void)print;
@end