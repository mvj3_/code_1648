//
//  Car.m
//  Learning0509
//
//  Created by  apple on 13-5-9.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "Car.h"
#import "Engine.h"
#import "Tire.h"

@implementation Car

@synthesize engine;

-(id)init{
    if(self = [super init]){
        engine = [[Engine alloc] init];
        
        tires = [NSMutableArray arrayWithCapacity:4];
         for(int i = 0 ; i < 4; i ++ ){
             [tires addObject:[NSNull null]];
         }
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone{
    Car * carCopy;
    carCopy = [[[self class] allocWithZone:zone] init];
    
    Engine * engineCopy;
    engineCopy = [[engine copy] autorelease];
    carCopy.engine = engineCopy;
    
    for( int i = 0 ; i < 4; i ++ ){
        Tire * tireCopy;
        tireCopy = [[self tireAtIndex:i] copy];
        [tireCopy autorelease];
        
        [carCopy setTire:tireCopy atIndex:i];
    }
    return carCopy;
}

-(void)setTire:(Tire *)tire atIndex:(int)index{
    if(index < 0 || index > 3){
        NSLog(@"bad index %d in tireAtIndex", index);
        exit(1);
    }
    [tires replaceObjectAtIndex:index withObject:tire];
}

-(Tire *)tireAtIndex:(int)index{
    if(index < 0 || index > 3){
        NSLog(@"bad index %d in tireAtIndex", index);
        exit(1);
    }
    return [tires objectAtIndex:index];
}


-(void)print{
    NSLog(@"%@", engine);
    
    for (int i = 0; i < 4; i ++ ) {
        NSLog(@"%@", [self tireAtIndex:i]);
    }
}
@end
